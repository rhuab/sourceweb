include(./config.pri)

TEMPLATE = subdirs
CONFIG += ordered

SUBDIRS += \
    third_party \
    libindexdb \
    clang-indexer

# HACK: Stop qmake from attempting to strip the scripts.
QMAKE_STRIP = /bin/echo

include(./check-clang.pri)
