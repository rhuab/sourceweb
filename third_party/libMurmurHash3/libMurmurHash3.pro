# MurmurHash3 SVN r147 (http://code.google.com/p/smhasher/)
include(../../config.pri)

QT -= core gui
CONFIG += static

TARGET = MurmurHash3
TEMPLATE = lib

SOURCES += MurmurHash3.cpp
HEADERS += MurmurHash3.h

target.path = $$LIB_DIR
INSTALLS += target

include(../../enable-cxx11.pri)
