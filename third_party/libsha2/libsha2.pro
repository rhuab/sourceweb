# Olivier Gay's SHA-2 library (http://www.ouah.org/ogay/sha2/)
include(../../config.pri)

QT -= core gui
CONFIG += static

TARGET = sha2
TEMPLATE = lib

SOURCES += sha2.c
HEADERS += sha2.h

target.path = $$LIB_DIR
INSTALLS += target
