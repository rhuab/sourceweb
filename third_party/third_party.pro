TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS += \
    libMurmurHash3 \
    libjsoncpp \
    libsha2 \
    libsnappy
